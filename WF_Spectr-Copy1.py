
# coding: utf-8

# In[3]:

def go2ref_spectr(ref):
    from libs import get_object, to_ref, r_sql, json_set
    import numpy as np
    ou=ref['$id']['$oid']
    obj = get_object(to_ref('ccord_data', ou))
    dev_object = get_object(obj['items']['dev_object'])
    [fS, fS_num, fS_log]=ParamFromObj_spectr(obj)
    [rig,WName] = RigisFromFile()
    [data,uWName] = SpectrFun(rig,WName,fS,fS_num,fS_log)


# In[1]:

def ParamFromObj_spectr(obj):
    tab=obj['items']['Spectrparam']

    fS=(tab[0]['spectr_cond_bool_id'], tab[1]['spectr_cond_bool_id'], tab[2]['spectr_cond_bool_id'],
        tab[3]['spectr_cond_bool_id'], tab[4]['spectr_cond_bool_id'], tab[5]['spectr_cond_bool_id'])

    fS_num=(tab[0]['spectr_cond_num_id'], tab[1]['spectr_cond_num_id'],tab[2]['spectr_cond_num_id'], 
            tab[3]['spectr_cond_num_id'], tab[4]['spectr_cond_num_id'],tab[5]['spectr_cond_num_id'])

    fS_log=(tab[0]['spectr_attr_use_id'], tab[1]['spectr_attr_use_id'],tab[2]['spectr_attr_use_id'], 
            tab[3]['spectr_attr_use_id'], tab[4]['spectr_attr_use_id'],tab[5]['spectr_attr_use_id'])
    return fS, fS_num, fS_log


# In[2]:

def RigisFromFile():
    oc = Oct2Py()
    dir(oc);
    oc.addpath(oc.genpath('./'))
    SD=oc.load('RIGIS_Byzz')
    rig=SD.rig;
    WellName=SD.WellName;
    return rig, WellName


# In[4]:

def SpectrFun(rig,WellName,fS,fS_num,fS_log):
    oc = Oct2Py()
    dir(oc);
    oc.addpath(oc.genpath('./'))
    K_min=fS_num[0]
    Lc_max=fS_num[1]
    W_max=fS_num[2]
    ENT_min=fS_num[3]
    Macr_max=fS_num[4]
    So0_min=fS_num[5]
    [data,unWellName]=oc.Spectr(rig,WellName,K_min,Lc_max,W_max,ENT_min,Macr_max,So0_min);
    return data, unWellName



# coding: utf-8

# In[1]:

from libs import get_object, to_ref, r_sql
import pandas as pd
import numpy as np


# In[2]:

from oct2py import Oct2Py, octave


# In[44]:

obj = get_object(to_ref('ccord_data', '586e073949f876002300d987'));
id=obj['_id']['$oid'];


# In[21]:

dev_object = get_object(obj['items']['dev_object'])
ds=dev_object['items']['data_start'];


# In[45]:

tab=obj['items']['Spectrparam']

fS=(tab[0]['spectr_cond_bool_id'], tab[1]['spectr_cond_bool_id'], tab[2]['spectr_cond_bool_id'],
    tab[3]['spectr_cond_bool_id'], tab[4]['spectr_cond_bool_id'], tab[5]['spectr_cond_bool_id'])

fS_num=(tab[0]['spectr_cond_num_id'], tab[1]['spectr_cond_num_id'],tab[2]['spectr_cond_num_id'], 
        tab[3]['spectr_cond_num_id'], tab[4]['spectr_cond_num_id'],tab[5]['spectr_cond_num_id'])

fS_log=(tab[0]['spectr_attr_use_id'], tab[1]['spectr_attr_use_id'],tab[2]['spectr_attr_use_id'], 
        tab[3]['spectr_attr_use_id'], tab[4]['spectr_attr_use_id'],tab[5]['spectr_attr_use_id'])

# print(fS)
# print(fS_num)
# print(fS_log)


# In[3]:

oc = Oct2Py()


# In[22]:

dir(oc);


# In[23]:

oc.addpath(oc.genpath('./'))


# In[26]:

SD=oc.load('RIGIS_Byzz')


# In[42]:

rig=SD.rig;
WellName=SD.WellName;
K_min=fS_num[0]
Lc_max=fS_num[1]
W_max=fS_num[2]
ENT_min=fS_num[3]
Macr_max=fS_num[4]
So0_min=fS_num[5]
[data,unWellName]=oc.Spectr(rig,WellName,K_min,Lc_max,W_max,ENT_min,Macr_max,So0_min);


# In[ ]:




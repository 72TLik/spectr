
function varargout = main(varargin)
% MAIN MATLAB code for main.fig
%      MAIN, by itself, creates a new MAIN or raises the existing
%      singleton*.
%
%      H = MAIN returns the handle to a new MAIN or the handle to
%      the existing singleton*.
%
%      MAIN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN.M with the given input arguments.
%
%      MAIN('Property','Value',...) creates a new MAIN or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main

% Last Modified by GUIDE v2.5 29-Dec-2016 16:08:16

% Begin initialization code - DO NOT EDIT
%gui_Singleton = 1;
%gui_State = struct('gui_Name',       mfilename, ...
%                   'gui_Singleton',  gui_Singleton, ...
%                   'gui_OpeningFcn', @main_OpeningFcn, ...
%                   'gui_OutputFcn',  @main_OutputFcn, ...
%                   'gui_LayoutFcn',  [] , ...
%                   'gui_Callback',   []);
%if nargin && ischar(varargin{1})
%    gui_State.gui_Callback = str2func(varargin{1});
%end

%if nargout
%    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
%else
%    gui_mainfcn(gui_State, varargin{:});
%end
% End initialization code - DO NOT EDIT


% --- Executes just before main is made visible.
function main_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main (see VARARGIN)

% Choose default command line output for main
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
test = uigetfile('*.xlsx','*.xls');
[num, txt, raw]= xlsread(test);

%raw = xlsread(test);
%imena = txt;
num_massiv = num;
txt_massiv = raw;
%save('geol_imena.mat','imena');
save('num_massiv.mat','num_massiv');
save('txt_massiv.mat','txt_massiv');
Names=txt_massiv(1,:); %���������
set(handles.popupmenu1, 'String', Names);
% UIWAIT makes main wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = main_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load('num_massiv.mat','num_massiv');
load('txt_massiv.mat','txt_massiv');

Names=txt_massiv(1,:); %���������
Vod_signal=num_massiv(:,6);
p=getGlobal;
at=num_massiv(:,p-2);

%set(handles.uitable1, 'data', num_massiv);
L=length(num_massiv(:,1));

 NGZ1=num_massiv(:,7); %��� ��������
 NGZ2=num_massiv(:,9);
 l=0;
for i=1:L%������� ����� ������ �������� ��� ����� �������������� �����������
    if Vod_signal(i)==0
        l=l+1;
        m_mat(l,1)=at(i);%����� ������ ���������� ��������
        NGZ3(l,1)=NGZ2(i);%����� ������ ��� �� �����������
               
    end
end

for i=1:l%����������� �� NaN 
    if isnan(m_mat(i))==1
        m_mat(i)=m_mat(i-1);
        
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%h = waitbar(0,'Please wait...');
%for i=1:L;
    %Proplast(i,1)=DownNotch(i) - UpNotch(i);
    %NGZ2(i,1) = NGZ1(i)*Proplast(i)/ENT(i)    %��� ����������
    %waitbar(0+i/30000,h,'Calculating...'); 
%end
%close(h) 
%save('NGZ2.mat','NGZ2');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 mat=median(m_mat);sat=sort(m_mat); mmat=mean(m_mat);
sNGZ3=sort(NGZ3);
n1=0;n2=0;%c�������
for i=1:l
        if sat(i)<mat
        X1_at(i)=sat(i);
        n1=n1+1;
        else
        X2_at(i)=sat(i);
        n2=n2+1;
        end 
end
% %X1,X2 - ������� ����� � ������ �� ������� � ������������� �������
% %��������������
Max=max(m_mat);
 m=fix(l/2);%������� ������� �� ��� �������
 j=fix(n1/4);k=fix(n2/4);%������� ������� ����������. j - ����� �������, k - ������ �������
 s_NGZ3=sum(NGZ3);%��������� ��� �� �����������
 y1=0;y2=0;y3=0;y4=0;y5=0;y6=0;y7=0;y8=0;
 x1=Max/7;x2=2*Max/7;x3=3*Max/7;x4=4*Max/7;x5=5*Max/7;x6=6*Max/7;
 x7=7*Max/7;%x8=sat(m+4*k);
% 
for i=1:l
    if m_mat(i)>0 && m_mat(i)<sat(j)
    y1=y1+NGZ3(i)*100/s_NGZ3;
    end
    if m_mat(i)>sat(j) && m_mat(i)<sat(2*j)
    y2=y2+NGZ3(i)*100/s_NGZ3;
    end
    if m_mat(i)>sat(2*j) && m_mat(i)<sat(3*j)
    y3=y3+NGZ3(i)*100/s_NGZ3;
    end
    if m_mat(i)>sat(3*j) && m_mat(i)<sat(4*j)
    y4=y4+NGZ3(i)*100/s_NGZ3;
    end
    if m_mat(i)>sat(m) && m_mat(i)<sat(m+k)
    y5=y5+NGZ3(i)*100/s_NGZ3;
    end
    if m_mat(i)>sat(m+k) && m_mat(i)<sat(m+2*k)
    y6=y6+NGZ3(i)*100/s_NGZ3;
    end
    if m_mat(i)>sat(m+2*k) && m_mat(i)<sat(m+3*k)
    y7=y7+NGZ3(i)*100/s_NGZ3;
    end
    if m_mat(i)>sat(m+3*k) 
    y8=y8+NGZ3(i)*100/s_NGZ3;
    end
end


% set(handles.uitable1, 'ColumnName', Names);

%subplot(3,1,1)
%hist(m_mat);
%subplot(3,1,2)
a=sat(m-3*j);
b=sat(m-2*j);
c=sat(m-j);
d=sat(m);
e=sat(m+k);
f=sat(m+2*k);
g=sat(m+3*k);
%figure
%axes
subplot(1,2,1);
plot([x1 x1], [0 y1],'r',[x2 x2],[0 y2],'r',[x3 x3],[0 y3],'b',[x4 x4],[0 y4],'b',[x5 x5],[0 y5],'b',[x6 x6],[0 y6],'b',[x7 x7],[0 y7],'b','LineWidth',20);
xlabel(Names(p));
ylabel('���� ���, %');
set( gca, 'xtick', 0 );
text1=num2str(a);text(x1,y1+1,text1,'FontSize',8);
text2=num2str(b);text(x2,y2+1,text2,'FontSize',8);
text3=num2str(c);text(x3,y3+1,text3,'FontSize',8);
text4=num2str(d);text(x4,y4+1,text4,'FontSize',8);
text5=num2str(e);text(x5,y5+1,text5,'FontSize',8);
text6=num2str(f);text(x6,y6+1,text6,'FontSize',8);
text7=num2str(g);text(x7,y7+1,text7,'FontSize',8);
subplot(1,2,2)
hist(m_mat);
xlabel(Names(p));
ylabel('����� ������������');




%x = {y1 y2 y3 y4 y5 y6 y7 y8};
%edges = [x1 x2 x3 x4 x5 x6 x7];
%h = histogram(x,edges);


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
setGlobal(get(hObject,'Value'));


function setGlobal(val)
global p
p = val;
function r = getGlobal
global p
r = p;
% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

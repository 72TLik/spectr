
# coding: utf-8

# In[1]:

import requests
import json

j_get = lambda r: json.loads(r.content.decode('utf8', 'ignore'))

def r_sql(func, data, instance='3'):
    return requests.post('http://demo.ccord.ru/sql%s/rpc/%s' % (instance, func), data=data)

def r_matlab(func, outCount, data=[]):
    headers = {'FunctionName': func, 'outCount': str(outCount), 'Content-type': 'application/json'}
    return requests.post('http://demo.ccord.ru/cosmos/expressm/v1/any', data=data, headers=headers)

cookies = requests.cookies.RequestsCookieJar()
cookies.set('ma_session', '3894d00b-31bd-4bef-9793-e5e4ce83e638')
def r_mongo(func, data={}):
    return requests.post('http://demo.ccord.ru/ma/%s' % (func), data=data, cookies=cookies)
def to_ref(ns, oid):
#     return "{'$ref':'%s','$id':{'$oid':'%s'}}" % (ns, oid)
    return {'$ref': ns,'$id':{'$oid': oid}}
def get_object(ref):
#     payload = "{'list':[%s]}" % ref
    payload = json.dumps({'list':[ref]}) 
    res = r_mongo('get_objects', payload)
    jdata = j_get(res)
    if 'error' in jdata:
        print(payload, jdata['error'])
    return res.json()["data"]["result"][0]

def json_get(ref, key):
    res = r_mongo('json_get', json.dumps({'ref': ref,"key": key}))
    return j_get(res)["data"]["json"]

def json_set(ref, key, val):
    r_mongo('json_set', json.dumps({'ref': ref,"key": key, "json": val}))

def set_surface(calc_ref,dev_object_ref,surface_type_ref,date,grid,surface):
    body = {
        'calc': calc_ref,
        'dev_object': dev_object_ref,
        'type': surface_type_ref,
        'date': date,
        'grid': grid,
        'surface': surface
    }
    res = r_mongo('surface_set', json.dumps(body))
    return j_get(res)

def get_surface(date, SuTyRef, calc_ref, dev_object_ref):
    #surface_type_ref = to_ref('cc_map', '5808441ba8caf6001792c985') #Поле давлений
    #surface_type_ref_g = to_ref('cc_map', '5808739ca8caf600178c2c5f') #Поле гидропроводности
    #dev_object_ref = to_ref('ccord', '58369d054d0048000b7ed41a') #Таплор
    #calc_ref = to_ref('ccord_data', '5836a8ad4d00480023178d9d') #"Вачинское - Косякова"
    body = {
        'calc': calc_ref,              # Ссылка на расчёт
        "dev_object": dev_object_ref,  # Ссылка на объект разработки
        "type": SuTyRef,               # Ссылка на тип поверхности
        "date": date                   # Дата
    }
    res = r_mongo('surface_get', json.dumps(body))
    return j_get(res)["data"]["obj"]["surface"]

def get_static_surface(SuTyRef, calc_ref, dev_object_ref):
    body = {
        'calc': calc_ref,              # Ссылка на расчёт
        "dev_object": dev_object_ref,  # Ссылка на объект разработки
        "type": SuTyRef                # Ссылка на тип поверхности
    }
    res = r_mongo('surface_get', json.dumps(body))
    try:
        out = j_get(res)["data"]["obj"]["surface"]
    except:
        out = res
    return out

def grid(x, y, z, resX=100, resY=100):
    from numpy import linspace, meshgrid
    from matplotlib.mlab import griddata

    "Convert 3 column data to matplotlib grid"
    xi = linspace(min(x), max(x), resX)
    yi = linspace(min(y), max(y), resY)
    Z = griddata(x, y, z, xi, yi)
    X, Y = meshgrid(xi, yi)
    return X, Y, Z


# In[2]:

#obj = get_object(to_ref('ccord_data', '5836d1f04d0048000a178db4'));


# In[2]:

# from libs import get_surface, get_static_surface, get_object, to_ref
def test_get_static_surface():
    calc_ref = to_ref('ccord_data', '5836a8ad4d00480023178d9d')
    calc_obj = get_object(calc_ref)
    dev_object_ref = calc_obj['items']['dev_object']
    rigisOnMap_ref = calc_obj['items']['rigisOnMap'];
    surface_type_ref = to_ref('cc_map', '682c18fc4d0048000c1de77d') #пористость
    return get_static_surface(surface_type_ref, rigisOnMap_ref, dev_object_ref)
# res = test_get_static_surface()
# res


# In[25]:

# dir(res)


# In[34]:

# res.json()['data']


# In[3]:

def tic():
    #Homemade version of matlab tic and toc functions
    import time
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()


# In[4]:

def toc():
    import time
    if 'startTime_for_tictoc' in globals():
        print( "Elapsed time is " + str(time.time() - startTime_for_tictoc) + " seconds.")
    else:
        print( "Toc: start time not set")


# In[ ]:




function [ data,uNames ] = Spectr(rig,Names,K_min,Lc_max,W_max,ENT_min,Macr_max,So0_min)
Names=Names';
uNames=unique(Names);
[~,idWell]=ismember(Names,uNames);

for i=1:size(uNames,1)
    f=find(idWell==i);
    up=rig(f,1);
    down=rig(f,2);
    H=down-up;
    ent=rig(f,3);
    So=rig(f,4);
    K=rig(f,5);
    ngzLayer=rig(f,6);
    ngzWell2=rig(f,7);
    ngzWell=sum(ngzLayer(So>0.45));
    
    Kent(i,1) = ves(K(So>0.45), H(So>0.45));
    W(i,1) = kontaktWater(So,up,down);
    ENT(i,1) = sum(ent(So>0.45));
    macrENT(i,1) = sum(So>0.45)/ENT(i,1);
    SoWell(i,1) = ves(So(So>0.45), H(So>0.45));
    L(i,1) = lorenc(K(So>0.45));
    
    v=(So>0.45);
    NGZ(i,1) = sum( ngzLayer(v.*(K<K_min)==1));
    NGZ(i,2) = sum( ngzLayer(v)).*( L(i,1) > Lc_max);
    NGZ(i,3) = sum( ngzLayer(v)).*( W(i,1) > W_max);
    NGZ(i,4) = sum( ngzLayer(v.*(ent<ENT_min)==1));
    NGZ(i,5) = sum( ngzLayer(v)).*(macrENT(i,1) > Macr_max);
    NGZ(i,6) = sum( ngzLayer(v.*(So<So0_min)==1));
end

data=[Kent L W ENT macrENT SoWell NGZ];

end

function A=ves(a,b)

A=sum(a.*b)/sum(b);
end

function W=kontaktWater(So,up,down)

f=So>0.45;
v=find(f);
if isempty(v)==0
    v1=v(end);
    if v1~=size(f,1)
        h=down-up;
        h2=up(2:end)-down(1:end-1);
        h2=[h2;0];
        if h2(v1)<2
            W=h(v1)/sum(h(f==1));
        else
            W=0;
        end
    else
        W=0;
    end
else
    W=0;
end

end

function L=lorenc(k)
if isempty(k)==0
    K=sum(k);
    n=size(k,1);
    for i=1:n
        x(i,:)=abs(k-k(i,1));
    end
    L=sum(x(:))/2/n/K;
else
    L=0;
end

end
